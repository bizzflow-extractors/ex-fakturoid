FROM alpine:latest
RUN apk add --no-cache python3
RUN python3 -m ensurepip
RUN python3 -m pip install --upgrade pip requests

VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

# Edit this any way you like it
ENTRYPOINT ["python3", "main.py"]

# Byl jsem tu, Tomik
