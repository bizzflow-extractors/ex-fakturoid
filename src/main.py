"""
Loads credentials from config.json file
Makes http request to get total number of pages for given endpoints
Makes http requests page by page
Downloads resulst for each endpoint into .csv file


Raises:
    FileNotFoundError: if config.json is not found
    Exception: if getting number of pages from headers fails

"""
import os
import csv
import re
import json
import requests
from logging import getLogger, basicConfig, INFO
from argparse import ArgumentParser
from datetime import datetime


start = datetime.now().timestamp()

OUTPUT_DIR = "/data/out/tables"
ENDPOINTS = ['expenses', 'invoices', 'subjects']

if not os.path.exists(OUTPUT_DIR):
    os.makedirs(OUTPUT_DIR)

## Parse arguments
argparser = ArgumentParser()
argparser.add_argument("-f", "--config", action="store", default="/config/config.json",
                       help="Filename of the config JSON,\ndefault /config/config.json")
argparser.add_argument("-l", "--loglevel", action="store",
                       default="INFO", choices=["INFO", "WARN", "ERROR", "DEBUG"])

args = argparser.parse_args()

basicConfig(level=INFO, format="[{asctime}] {levelname}: {message}", style="{")
logger = getLogger(__name__)
logger.setLevel(args.loglevel)

## Config file
if not os.path.exists(args.config):
    logger.error("{0} was not found".format(args.config))
    raise FileNotFoundError("{0} was not found".format(args.config))

with open(args.config, "r") as conf_file:
    conf = json.load(conf_file)

## Get last page number
def get_last_page_num(endpoint):
    """
    Makes http request
    Checks link for validity
    Gets last page number from headers
    
    Arguments:
        endpoint {str} -- endpoint to download
    
    Raises:
        Exception: if getting number of pages by regex from link fails
    
    Returns:
        [int] -- total number of pages
    """
    url = "{0}{1}.json".format(conf['base_url'], endpoint)
    response = requests.get(url, headers={'Accept':'application/json'},auth=(conf['username'], conf['api_key']))
    
    if "Link" in response.headers.keys():
        s = (response.headers["Link"])
        logger.info("getting last page number for {0} from:\n{1}".format(endpoint,s))
        result = re.search('(?<=page\=)(.*)(?=>; rel\="last")', s)
        if result is None:
            logger.error("failed to get number of pages from:\n{0}".format(s))
            raise Exception("unexpected link")
        else:
            return int(result.group(1))
    else:
        return 1

## Save output into csv
def write_csv (endpoint, json_array):
    """
    Saves json array into csv
    
    Arguments:
        endpoint {str} -- endpoint
        json_array {list} -- list of dictionaries
    """
    output_filename = os.path.join(OUTPUT_DIR, "{0}.csv".format(endpoint))
    logger.info("Writing out file {0}".format(output_filename))
    try:
        with open(output_filename, "w", encoding="utf-8") as outfile:
            writer = csv.DictWriter(outfile, fieldnames=json_array[0].keys(), dialect=csv.unix_dialect)
            writer.writeheader()
            for row in json_array:
                writer.writerow(row)
    except:
        logger.error("Error when writing file")


## Download data
def download_endpoint(endpoint, max_page):
    """
    Makes http request
    Saves results into .csv
    Saves results from nested list into separate .csv if lines are in headers
    
    Arguments:
        endpoint {str} -- Endpoint to download
        max_page {int} -- last page number for fiven endpoint
    """
    json_array = []
    json_array_lines = []
    
    for page in range(1, max_page + 1):
        url = "{0}{1}.json?page={2}".format(conf['base_url'], endpoint, page)
        logger.info("GET request - {0}, page {1}/{2}, url: {3}".format(endpoint, page, max_page, url))
        response = requests.get(url, headers={'Accept':'application/json'},auth=(conf['username'], conf['api_key']))
        tmp_json_array = response.json()      

        try: 
            response_keys = tmp_json_array[0].keys()
            if not 'lines' in response_keys:
                json_array.extend(tmp_json_array)
            else:
                for f in tmp_json_array:
                    for line in f["lines"]:
                        current = line
                        current["invoice_id"] = f["id"]
                        json_array_lines.append(current)
                    del f['lines']
                    json_array.append(f)
        except:
            logger.error("No keys in empty array")

    if endpoint == 'subjects':
        write_csv(endpoint, json_array)
    else:
        write_csv(endpoint, json_array)
        write_csv(endpoint + "_lines", json_array_lines)


## Execution
for e in ENDPOINTS:
    pages = get_last_page_num(e)
    download_endpoint(e, pages)

now = datetime.now()
end = now.timestamp()
runtime_sec = round(end - start, 2)

logger.info("finished at {0}, runtime: {1} seconds".format(now, runtime_sec))

