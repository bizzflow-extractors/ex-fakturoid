# Name of the Project

* **autor:** Jiri Tobolka (jiri.tobolka@bizztreat.com)
* **created:** 2019-08-31

## Description

Extractor downloads the data from Fakturoid App. 

### What it is used for

It downloads the Expenses, Invoices and Subjects from your App.

### What the code does

The code does:

1. Extracts entities
2. Store them as CSV files

## Requirements

Requerements for running this project or the {python_name.py} file in are:

* Have your API activated in Fakturoid
* Know your API key

## How to use it

Setup the config file:

```
{
"username":"USERNAME",
"base_url":"https://app.fakturoid.cz/api/v2/accounts/COMPANY/",
"api_key":"API_KEY"
}
```

### TO_DO

* incremental loading - using `Last-Modified`, `If-None-Match`, and `If-Modified-Since` headers, further info: [API documentaiton](https://fakturoid.docs.apiary.io/#introduction/identifikace-vasi-aplikace)
